# Rebeca García Mencía
# !/usr/bin/python3
# -*- coding: utf-8 -*-

"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import json
import os
import random
import socket
import socketserver
import sys
import random
import hashlib
import json
from time import gmtime, strftime, strptime, time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from uaclient import log

CONFIG = sys.argv[1]


class XMLHandler(ContentHandler):
    """Clase XML."""

    def __init__(self):
        """Declarar diccionarios."""
        self.lista = []
        self.etiquetas = {
            "server": ["name", "ip", "puerto"],
            "database": ["path", "passwdpath"],
            "log": ["path"],
            }

    def startElement(self, name, attrs):
        """Guarda los datos en el diccionario."""
        if name in self.etiquetas:
            dicc = {}
            dicc["etiqueta"] = name
            for valor in self.etiquetas[name]:
                dicc[valor] = attrs.get(valor, "")
            self.lista.append(dicc)

    def get_tags(self):
        """Extrae los datos."""
        return self.lista


class ProxyHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    IP_dicc = {}
    Passwd_dicc = {}
    nonce_dicc = {}

    def register2json(self):
        """Creación de fichero json."""
        fichero_json = "registered.json"

        with open(fichero_json, "w") as fichero:
            json.dump(self.IP_dicc, fichero, indent=4)

    def json2registered(self):
        """Comprobar si hay fichero registered.json."""
        try:
            with open("registered.json", "r") as fichero:
                self.dicc_IP = json.load(fichero)
        except:
            pass

    def exp(self):
        """Comprobar expirados."""
        exp_list = []
        tiempo_actual = strftime('%Y-%m-%d %H:%M:%S', gmtime(time()))
        for name in self.IP_dicc:
            if tiempo_actual > str(self.IP_dicc[name][2]):
                exp_list.append(name)
        for name in exp_list:
            del self.IP_dicc[name]

    def password_json(self, fichero):
        """Comprobar contraseñas."""
        try:
            with open("passwords.json", "r") as fichero:
                self.Passwd_dicc = json.load(fichero)
        except:
            pass

    def handle(self):
        """Metodo que gestiona las peticiones."""
        self.json2registered()
        IP_CLIENT = self.client_address[0]
        PORT_CLIENT = self.client_address[1]
        line = self.rfile.read()
        decodificar = line.decode('utf-8')
        print("El cliente nos manda ", decodificar)
        separador1 = decodificar.split()
        METHOD = separador1[0]
        lista_metodos = ["REGISTER", "INVITE", "ACK", "BYE"]
        log(LOG, "Received from", IP_CLIENT, PORT_CLIENT, decodificar)

        print("Recibido: " + decodificar)

        if METHOD == "REGISTER":
            # Comprobar valor expires y nonce:
            decodificar = line.decode('utf-8').split()
            EXPIRES = int(separador1[4])
            USER = separador1[1].split(":")[1]
            PORT = separador1[1].split(":")[-1]
            IP_CLIENT = self.client_address[0]
            expired = EXPIRES + time()
            end_time = strftime('%Y-%m-%d %H:%M:%S', gmtime(expired))
            self.IP_dicc[USER] = ["address: " + IP_CLIENT, "port: " + PORT, "expires: " + end_time]

            if EXPIRES == 0:
                try:
                    if USER in self.IP_dicc:
                        del self.IP_dicc[USER]
                        self.register2json()
                        MESSAGE = "SIP/2.0 200 OK\r\n\r\n"
                        self.wfile.write(bytes(MESSAGE, 'utf-8'))
                        log(LOG, "Send to", IP_CLIENT, PORT_CLIENT, MESSAGE)
                except KeyError:
                    self.wfile.write(b"SIP/2.0 404 User Not Found\r\n\r\n")

            elif EXPIRES != 0 and "Digest" not in decodificar:

                n = str(random.randint(000000000, 999999999))
                self.nonce_dicc[USER] = n
                print(n)
                MESSAGE = "SIP/2.0 401 Unauthorized\r\n"
                MESSAGE += "WWW-Authenticate: Digest nonce="
                MESSAGE += self.nonce_dicc[USER] + "\r\n\r\n"

                self.wfile.write(bytes(MESSAGE, 'utf-8'))
                log(LOG, "Send to", IP_CLIENT, PORT_CLIENT, MESSAGE)

                print("Envío: " + MESSAGE)

            elif EXPIRES != 0 and "Digest" in decodificar:
                USER = separador1[1].split(":")[1]
                PORT = separador1[1].split(":")[2]
                EXPIRES = int(separador1[4])
                response = separador1[-1].split("=")[1]
                expired = EXPIRES + time()
                end_time = strftime('%Y-%m-%d %H:%M:%S', gmtime(expired))
                self.password_json(self)
                passwd = self.Passwd_dicc[USER]
                print(passwd)
                # Comparación contraseñas
                h = hashlib.md5()
                m = self.nonce_dicc[USER]
                h.update(bytes(m, 'utf-8'))
                h.update(bytes(passwd, 'utf-8'))
                h.digest()
                r_nonce = h.hexdigest()

                if response == r_nonce:
                    self.IP_list = []
                    self.IP_list.append(IP_CLIENT)
                    self.IP_list.append(PORT)
                    self.IP_list.append(end_time)
                    self.IP_dicc[USER] = self.IP_list
                    self.IP_list = []
                    self.exp()
                    self.register2json()

                    MESSAGE = "SIP/2.0 200 OK"
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                    log(LOG, "Send to", IP_CLIENT, str(PORT), MESSAGE)

                    print("Envío: " + MESSAGE)

                else:
                    print("Contraseña Errónea")

                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                        my_socket.connect((IP_CLIENT, PORT_CLIENT))
                        MESSAGE = "SIP/2.0 400 Bad Request\r\n\r\n"
                        self.wfile.write(bytes(MESSAGE, 'utf-8'))
                        log(LOG, "Error", IP_CLIENT, str(PORT), "")
                self.register2json()

        elif METHOD == "INVITE":
            USER = separador1[1].split(":")[1]
            print(USER)
            RTP_PORT = separador1[-2]

            if USER in self.IP_dicc:
                # Valores de IP y Puerto extraídos del diccionario de clientes
                IP = self.IP_dicc[USER][0]
                PORT = self.IP_dicc[USER][1]
                try:
                    # Creación de socket y atar a servidor:
                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                        my_socket.connect((IP, int(PORT)))

                        print("Envío: " + decodificar)
                        my_socket.send(bytes(decodificar, 'utf-8') + b"\r\n")
                        log(LOG, "Send to", IP, PORT, decodificar)

                        data = my_socket.recv(int(PORT))
                        decodificar = data.decode('utf-8')
                        self.wfile.write(bytes(decodificar, 'utf-8'))
                        log(LOG, "Received from", IP, PORT, decodificar)
                        print("Recibo: ", decodificar)

                except ConnectionRefusedError:
                        print("ERROR: No server listening at " + str(IP) + " at port: " + str(PORT) + "\r\n\r\n")

                        log(LOG, "Error", IP, PORT, "")
            else:
                MESSAGE = "SIP/2.0 404 User Not Found\r\n\r\n"
                self.wfile.write(bytes(MESSAGE, 'utf-8'))
                log(LOG, "Send to", IP_CLIENT, PORT_CLIENT, MESSAGE)
                print("Envío: " + decodificar)

        elif METHOD == "ACK":
            USER = separador1[1].split(":")[1]
            # Valores de IP y Puerto extraídos del diccionario de clientes
            IP = self.IP_dicc[USER][0]
            PORT = self.IP_dicc[USER][1]

            log(LOG, "Received from", IP, PORT, decodificar)

            try:
                # Creación de socket y atar a servidor:
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    my_socket.connect((IP, int(PORT)))

                    print("Envío: \r\n" + decodificar)
                    my_socket.send(bytes(decodificar, 'utf-8') + b"\r\n")
                    log(LOG, "Send to", IP, PORT, decodificar)

            except ConnectionRefusedError:
                print("ERROR: No server listening at " + str(IP) + " at port: " + str(PORT) + "\r\n\r\n")
                log(LOG, "Error", IP, PORT, "")

        elif METHOD == "BYE":

            # Valores de IP y Puerto extraídos del diccionario de clientes
            USER = separador1[1].split(":")[1]
            IP = self.IP_dicc[USER][0]
            PORT = self.IP_dicc[USER][1]

            try:
                # Creación de socket y atar a servidor:
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    my_socket.connect((IP, int(PORT)))

                    print("Envío: " + decodificar)
                    my_socket.send(bytes(decodificar, 'utf-8') + b"\r\n")
                    log(LOG, "Send to", IP, PORT, decodificar)

                    data = my_socket.recv(int(PORT))
                    decodificar = data.decode('utf-8')
                    self.wfile.write(bytes(decodificar, 'utf-8'))
                    print(decodificar)
                    log(LOG, "Received from", IP, PORT, decodificar)
                    log(LOG, "Send to", IP_CLIENT, "", decodificar)
                    print("Envío: " + decodificar)

            except ConnectionRefusedError:
                print("ERROR: No server listening at " + str(IP) + " at port: " + str(PORT) + "\r\n\r\n")
                log(LOG, "Error", IP, PORT, "")

        elif METHOD not in lista_metodos:
            MESSAGE = "SIP/2.0 405 Method Not Allowed\r\n\r\n"
            self.wfile.write(bytes(MESSAGE, 'utf-8'))
            log(LOG, "Error", IP_CLIENT, PORT_CLIENT, MESSAGE)
            print("Envío: " + MESSAGE)

        else:
            self.json2registered()

            MESSAGE = "SIP/2.0 400 Bad Resquest\r\n\r\n"
            self.wfile.write(bytes(MESSAGE, 'utf-8'))
            log(LOG, "Error", IP, PORT, "")


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("Usage: proxy_registrar.py config")

    # Parser de la clase XMLHandler
    parser = make_parser()
    PRHandler = XMLHandler()
    parser.setContentHandler(PRHandler)
    parser.parse(open(CONFIG))
    PR = PRHandler.get_tags()

    # Extraer datos del fichero XML
    PROXY_NAME = PR[0]["name"]
    IP_SERVER = PR[0]["ip"]
    PORT_SERVER = PR[0]["puerto"]
    DATABASE = PR[1]["path"]
    PASSWORD = PR[1]["passwdpath"]
    LOG = PR[2]["path"]

    try:
        serv = socketserver.UDPServer((IP_SERVER, int(PORT_SERVER)), ProxyHandler)
        print("Proxy-Server " + PROXY_NAME + " listening at port " + PORT_SERVER + "...")
        serv.serve_forever()

    except KeyboardInterrupt:
        sys.exit("Finalizado Proxy")
