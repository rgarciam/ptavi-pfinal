# Rebeca García Mencía
# !/usr/bin/python3
# -*- coding: utf-8 -*-

"""Clases y programa principal cliente."""

import sys
import socket
import os
import time
import hashlib
from xml.sax.handler import ContentHandler
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from time import gmtime, strftime, strptime, time


def log(fichero, event, ip, port, mensaje):
    """Escribe en el fichero log."""
    fichero = open("log.txt", "a+")
    t = strftime("%Y%m%d%H%M%S", gmtime(time()))

    if event == "Error":
        content = t + " " + event + ": No server listening at " + str(ip) + " port " + str(port) + "\r\n"
    elif event == "Send to" or event == "Received from":
        content = t + " " + event + " " + str(ip) + ":" + str(port) + ":" + " " + mensaje + "\r\n"
    else:
        content = t + " " + event + "\r\n"

    fichero.write(content)
    fichero.close()


class XMLHandler(ContentHandler):
    """Clase XML."""

    def __init__(self):
        """Declarar diccionarios."""
        self.lista = []
        self.etiquetas = {
            "account": ["username", "password"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
            }

    def startElement(self, name, attrs):
        """Guarda los datos en el diccionario."""
        if name in self.etiquetas:
            dicc = {}
            dicc['etiqueta'] = name
            for valor in self.etiquetas[name]:
                dicc[valor] = attrs.get(valor, "")
            self.lista.append(dicc)

    def get_tags(self):
        """Extraer los datos."""
        return self.lista

    def system(self):
        """Se define el programa que envia en MP3."""
        aEjecutar = "./mp32rtp -i " + self.RTP[1] + "-p" + self.RTP[2]
        aEjecutar += " < " + AUDIO
        print("Vamos a ejecutar", aEjecutar)
        os.system(aEjecutar)


if __name__ == "__main__":
    """Programa principal."""

    if len(sys.argv) != 4:
        sys.exit("Usage: python3 uaclient.py config method option")

    CONFIG = sys.argv[1]
    METHOD = sys.argv[2]
    OPTION = sys.argv[3]
    lista_metodos = ["REGISTER", "INVITE", "BYE"]

    # Parser de la clase XMLHandler

    parser = make_parser()
    UCHandler = XMLHandler()
    parser.setContentHandler(UCHandler)
    parser.parse(open(CONFIG))
    UC = UCHandler.get_tags()

    # Extraer datos del fichero XML
    USERNAME = UC[0]['username']
    PASSWORD = UC[0]['password']
    IP_SERVER = UC[1]['ip']
    PORT_SERVER = UC[1]['puerto']
    RTP_PORT = UC[2]['puerto']
    IP_PROXY = UC[3]['ip']
    PORT_PROXY = int(UC[3]['puerto'])
    LOG = UC[4]['path']
    AUDIO = UC[5]['path']

    if METHOD == lista_metodos[0]:
        MESSAGE = METHOD + " sip:" + USERNAME + ":" + PORT_SERVER + " SIP/2.0" + "\r\n"
        MESSAGE += "Expires: " + OPTION + "\r\n\r\n"
        log(LOG, "Send to", IP_PROXY, PORT_PROXY, MESSAGE)
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                my_socket.connect((IP_PROXY, PORT_PROXY))
                my_socket.send(bytes(MESSAGE, 'utf-8'))
                data = my_socket.recv(PORT_PROXY)
                print(data.decode('utf-8'))

        except ConnectionRefusedError:
            sys.exit("ERROR: No server listening at " + IP_PROXY + " at port: " + str(PORT_PROXY) + "\r\n\r\n")
            log(LOG, "Error", IP_PROXY, PORT_PROXY, "")

    elif METHOD == lista_metodos[1]:

        MESSAGE = METHOD + " sip:" + OPTION + " SIP/2.0" + "\r\n"
        MESSAGE += "Content-type: application/sdp\r\n\r\n"
        MESSAGE += "v = 0\r\n"
        MESSAGE += "o = " + USERNAME + " " + IP_SERVER + "\r\n"
        MESSAGE += "s = misesion\r\n"
        MESSAGE += "t = 0" + "\r\n"
        MESSAGE += "m = audio " + RTP_PORT + " RTP" + "\r\n"
        log(LOG, "Send to", IP_PROXY, PORT_PROXY, MESSAGE)
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                my_socket.connect((IP_PROXY, PORT_PROXY))
                my_socket.send(bytes(MESSAGE, 'utf-8'))
                data = my_socket.recv(1024)
                print(data.decode('utf-8'))

        except ConnectionRefusedError:
            sys.exit("ERROR: No server listening at " + IP_PROXY + " at port: " + str(PORT_PROXY) + "\r\n\r\n")
            log(LOG, "Error", IP_PROXY, PORT_PROXY, "")

    elif METHOD == lista_metodos[2]:
        MESSAGE = METHOD + " sip:" + OPTION + " SIP/2.0" + "\r\n\r\n"
        log(LOG, "Send to", IP_PROXY, PORT_PROXY, MESSAGE)
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                my_socket.connect((IP_PROXY, PORT_PROXY))
                my_socket.send(bytes(MESSAGE, 'utf-8'))
                data = my_socket.recv(1024)
                print(data.decode('utf-8'))

        except ConnectionRefusedError:
            sys.exit("ERROR: No server listening at " + IP_PROXY + " at port: " + str(PORT_PROXY) + "\r\n\r\n")
            log(LOG, "Error", IP_PROXY, PORT_PROXY, "")

    elif METHOD not in lista_metodos:
        ERROR = "SIP/2.0 405 Method Not Allowed"
        print(ERROR)
        log(LOG, "Error", USERNAME, PORT_SEVER, ERROR)

    # Respuesta del proxy
    recibo = data.decode('utf-8')
    respuesta = data.decode('utf-8').split()
    log(LOG, "Received from", str(IP_PROXY), str(PORT_PROXY), recibo)

    if respuesta[1] == "401":
        # REGISTER con autenticación
        NONCE = respuesta[-1].split("=")[1]
        h = hashlib.md5()
        h.update(bytes(NONCE, 'utf-8'))
        h.update(bytes(PASSWORD, 'utf-8'))
        h.digest
        r_nonce = h.hexdigest()

        M_REGISTER = METHOD + " sip:" + USERNAME + ":" + PORT_SERVER
        M_REGISTER += " SIP/2.0\r\n" + 'Expires: ' + OPTION + "\r\n"

        MESSAGE = M_REGISTER + "WWW-Authenticate: Digest response="
        MESSAGE += r_nonce + "\r\n\r\n"

        print(MESSAGE)

        # Envío autenticación
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((IP_PROXY, PORT_PROXY))
            my_socket.send(bytes(MESSAGE, 'utf-8'))

            log(LOG, "Send to", IP_PROXY, PORT_PROXY, MESSAGE)

    if respuesta[1] == "100":
        log(LOG, "Received from", IP_PROXY, PORT_PROXY, recibo)
        # Envío ACK
        M_ACK = "ACK" + " sip:" + OPTION + " SIP/2.0" + "\r\n\r\n"

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((IP_PROXY, PORT_PROXY))
            my_socket.send(bytes(M_ACK, 'utf-8'))
            log(LOG, "Send to", IP_PROXY, PORT_PROXY, M_ACK)

            # Enviar RTP:
            aEjecutar = "./mp32rtp -i " + IP_SERVER + " -p " + RTP_PORT
            aEjecutar += " < " + AUDIO
            print("Vamos a ejecutar", aEjecutar)
            os.system(aEjecutar)
            print("Enviando audio", aEjecutar)
            log(LOG, "Audio transfer completed", "", "", "")

    elif respuesta[1] == "200":

        log(LOG, "Finishing...", "", "", "")
        my_socket.close()
    else:
        my_socket.close()
