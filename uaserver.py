#  Rebeca García Mencía
#  !/usr/bin/python3
#  -*- coding: utf-8 -*-

"""Clases y programa principal servidor."""

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import os
import socket
import socketserver
import sys
from uaclient import log

CONFIG = sys.argv[1]


class XMLHandler(ContentHandler):
    """Clase XML."""

    def __init__(self):
        """Declarar diccionarios."""
        self.lista = []
        self.etiquetas = {
            "account": ["username", "password"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
            }

    def startElement(self, name, attrs):
        """Guarda los datos en el diccionario."""
        if name in self.etiquetas:
            dicc = {}
            dicc['etiqueta'] = name
            for valor in self.etiquetas[name]:
                dicc[valor] = attrs.get(valor, "")
            self.lista.append(dicc)

    def get_tags(self):
        """Extraer los datos."""
        return self.lista


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    RTP = []

    def handle(self):
        """
        Handle method of the server class.

        (all requests will be handled by this method).
        """
        lista_metodos = ["INVITE", "ACK", "BYE"]
        IP_CLIENT = str(self.client_address[0])
        PORT_CLIENT = str(self.client_address[1])
        line = self.rfile.read()
        decodificar = line.decode('utf-8')
        print("El cliente nos manda ", decodificar)
        METHOD = decodificar.split(" ")[0]
        comprobar1 = decodificar.split(" ")[2]

        # INVITE
        if METHOD == lista_metodos[0]:
            log(LOG, "Received from", IP_PROXY, PORT_PROXY, decodificar)
            MESSAGE = "SIP/2.0 100 Trying\r\n\r\n"
            MESSAGE += "SIP/2.0 180 Ringing\r\n\r\n SIP/2.0 200 OK\r\n\r\n"
            MESSAGE += "Content-type: application/sdp\r\n\r\n"
            MESSAGE += "v = 0\r\n"
            MESSAGE += "o = " + USERNAME + " " + IP_SERVER + "\r\n"
            MESSAGE += "s = misesion\r\n"
            MESSAGE += "t = 0\r\n"
            MESSAGE += "m= audio " + RTP_PORT + " RTP" + "\r\n\r\n"
            self.wfile.write(bytes(MESSAGE, 'utf-8'))

            self.RTP.append(IP_SERVER)
            self.RTP.append(RTP_PORT)

            log(LOG, "Send to", IP_PROXY, PORT_PROXY, MESSAGE)
            print("Send:" + MESSAGE)

        # ACK
        elif METHOD == lista_metodos[1]:
            log(LOG, "Received from", IP_PROXY, PORT_PROXY, decodificar)
            print("Enviando archivo de audio")
            # Enviar RTP:
            aEjecutar = "./mp32rtp -i " + self.RTP[0] + " -p " + self.RTP[1]
            aEjecutar += " < " + AUDIO
            print("Vamos a ejecutar", aEjecutar)
            os.system(aEjecutar)
            print("Enviando audio", aEjecutar)
            log(LOG, "Audio transfer completed", "", "", "")

        # BYE
        elif METHOD == lista_metodos[2]:
            log(LOG, "Received from", IP_PROXY, PORT_PROXY, decodificar)
            MESSAGE = "SIP/2.0 200 OK\r\n\r\n"
            self.wfile.write(bytes(MESSAGE, 'utf-8'))
            log(LOG, "Send to", IP_PROXY, PORT_PROXY, decodificar)
            print("Send: " + MESSAGE)

        elif METHOD not in lista_metodos:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
            ERROR = "SIP/2.0 405 Method Not Allowed"
            log(LOG, "Error", IP_CLIENT, PORT_CLIENT, ERROR)

        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
            ERROR = "SIP/2.0 400 Bad Request"
            log(LOG, "Error", IP_CLIENT, PORT_CLIENT, ERROR)


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("Usage: python3 uaserver.py config")

    # Parser de la clase XMLHandler
    parser = make_parser()
    USHandler = XMLHandler()
    parser.setContentHandler(USHandler)
    parser.parse(open(CONFIG))
    US = USHandler.get_tags()

    # Extraer datos del fichero XML

    USERNAME = US[0]['username']
    PASSWORD = US[0]['password']
    IP_SERVER = US[1]['ip']
    PORT_SERVER = US[1]['puerto']
    RTP_PORT = US[2]['puerto']
    IP_PROXY = US[3]['ip']
    PORT_PROXY = int(US[3]['puerto'])
    LOG = US[4]['path']
    AUDIO = US[5]['path']

    serv = socketserver.UDPServer((IP_SERVER, int(PORT_SERVER)), EchoHandler)
    print("Listening...")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        log(LOG, "Finishing...", IP_PROXY, PORT_PROXY, "")
        print("finalizado server")
